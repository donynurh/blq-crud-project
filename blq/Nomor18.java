public class Nomor18 {
    public static void main(String[] args) {

        int[] jam = {9, 13, 15, 17};
        int[] kalori = {30, 20, 50, 80};
        int startWorkout = 18, diff;
        double workoutTime = 0;

        for (int i = 0; i < jam.length; i++) {
            diff = startWorkout - jam[i];
            workoutTime += 0.1*kalori[i]*diff;
        }

        int drinkWater = 500;
        if (workoutTime > 30) {
            drinkWater += (int) (100 * Math.floor(workoutTime / 30));
        }

        System.out.println("Donna akan meminum air sebanyak "+ drinkWater +" cc sepanjang berolahraga selama "+ workoutTime + " menit");
    }
}
